/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenempleado;

/**
 *
 * @author marco
 */
public class Docente extends Empleado{
    
    private int nivel;
    private int horas;
    private float pagoHora;
    
    public Docente(){
        this.nivel = 0;
        this.horas = 0;
        this.pagoHora = 0.0f;
        
    }

    public Docente(int nivel, int horas, float pagoHora, int numero, String nombre, String domicilio, Contrato contrato, int clave, String puesto) {
        super(numero, nombre, domicilio, contrato, clave, puesto);
        this.nivel = nivel;
        this.horas = horas;
        this.pagoHora = pagoHora;
    }

    

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }

    public float getPagoHora() {
        return pagoHora;
    }

    public void setPagoHora(float pagoHora) {
        this.pagoHora = pagoHora;
    }
    
    
    public float calcularTotal(){
        float totalPago = 0.0f;
        totalPago = this.calcularPago()+ this.calcularImpuesto();
        return totalPago;
    }
    
    public float calcularAdicional(){
        float adicional = 0.0f;
        switch(this.nivel){
            case 1:
                adicional = calcularPago()*.35f;
            case 2:
                adicional = calcularPago()*.40f;
            case 3:
                adicional = calcularPago()*.50f;
        }
        return adicional;
    }

    @Override
    public float calcularImpuesto() {
        return this.horas * this.pagoHora * this.contrato.getImpuesto()/100;
    }

    @Override
    public float calcularPago() {
       float pago = 0.0f;
       pago = this.horas * this.pagoHora;
       return pago;
    }    
}
