/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenempleado;

/**
 *
 * @author marco
 */
public abstract class Empleado{
    protected int numero;
    protected String nombre;
    protected String domicilio;
    protected Contrato contrato;
    

    public Empleado() {
        this.numero = 0;
        this.nombre = "";
        this.domicilio = "";
    }

    public Empleado(int numero, String nombre, String domicilio,Contrato contrato, int clave, String puesto) {
        this.numero = numero;
        this.nombre = nombre;
        this.domicilio = domicilio;
    }


    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }
    public abstract float calcularPago();
    public abstract float calcularImpuesto();
    
}
